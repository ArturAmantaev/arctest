package easyTasks;

import java.math.BigInteger;
import java.util.Scanner;

class FibonachiWithRe {
    public static BigInteger fibonachi(BigInteger n) {
        if (n.compareTo(BigInteger.valueOf(0)) == 0) {
            return null;
        } else if (n.compareTo(BigInteger.valueOf(1)) == 0) {
            return BigInteger.valueOf(0);
        } else if (n.compareTo(BigInteger.valueOf(2)) == 0) {
            return BigInteger.valueOf(1);
        } else{
            BigInteger number_f = (fibonachi(n.subtract(BigInteger.valueOf(1)))).add(fibonachi(n.subtract(BigInteger.valueOf(2))));
            return number_f;
        }
    }
}
