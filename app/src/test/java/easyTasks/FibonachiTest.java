package easyTasks;

import org.junit.jupiter.api.Test;

import java.math.BigInteger;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FibonachiTest {
    @Test void fibonachi() {
        assertEquals("0 1 1 2 3 5 8 13 21 34 55 89 ", Fibonachi.fibonachi(12));
    }

    @Test void fibonachiOne() {
        assertEquals("0 ", Fibonachi.fibonachi(1));
    }

    @Test void fibonachiNull() {
        assertEquals("", Fibonachi.fibonachi(0));
    }

    @Test void fibonachiRe() {
        BigInteger n1 = BigInteger.valueOf(12);
        BigInteger n2 = BigInteger.valueOf(89);
        assertEquals(n2, FibonachiWithRe.fibonachi(n1));
    }

    @Test void fibonachiReOne() {
        BigInteger n1 = BigInteger.valueOf(1);
        BigInteger n2 = BigInteger.valueOf(0);
        assertEquals(n2, FibonachiWithRe.fibonachi(n1));
    }

    @Test void fibonachiReNull() {
        BigInteger n1 = BigInteger.valueOf(0);
        assertEquals(null, FibonachiWithRe.fibonachi(n1));
    }
}
